" Vim color file
" Maintainer: Andy Hurd <andy.d.hurd@gmail.com>
" Contributors:
" Last Change: 2013/11/7
"
" Extension of: "newsprint" by
" Maintainer:	Steven Stallion <sstallion@gmail.com>
" Contributors:
" Last Change:	2010/10/21

set cursorline

highlight clear
if exists("syntax_on")
	syntax reset
endif

let g:colors_name = "dapper"

" Console Colors
hi Comment      ctermfg=black
hi Constant     ctermfg=red     cterm=bold
hi Error        ctermfg=grey    cterm=bold            ctermbg=black
hi Identifier   ctermfg=black   cterm=bold
hi Normal       ctermfg=grey
hi PreProc      ctermfg=black   cterm=bold
hi Special      ctermfg=black   cterm=bold
hi Statement    ctermfg=white   cterm=bold
hi Type         ctermfg=166     cterm=bold
hi Visual       ctermfg=white                         ctermbg=red
hi CursorLine   ctermbg=232     cterm=bold
hi CursorLineNR ctermfg=166     cterm=bold            ctermbg=232
hi LineNr       ctermfg=233     cterm=bold
hi ColorColumn	ctermbg=232
hi MatchParen	  ctermfg=10                            ctermbg=232

" GUI Colors
hi Comment	guifg=grey
hi Constant 	guifg=grey			gui=bold
hi Cursor			guibg=black
hi CursorLine 	guibg=grey
hi Error		guifg=grey	guibg=black	gui=bold
hi Identifier 	guifg=black			gui=bold
hi Normal 	guifg=black
hi PreProc 	guifg=black			gui=italic
hi Special	guifg=black			gui=bold
hi Statement 	guifg=black			gui=bold,underline
hi Type 		guifg=black			gui=bold
hi Visual	guifg=black	guibg=grey

" Links
hi! link Search		Visual
hi! link NonText 	Normal
hi! link Macro		PreProc
hi! link Boolean		Constant
hi! link Character	Constant
hi! link Conditional	Statement
hi! link CursorColumn	CursorLine
hi! link cssIdentifier Statement
hi cssTagName ctermfg=grey cterm=bold
hi! link cssPseudoClassId Type
hi! link cssFontAttr Constant
hi! link cssTextAttr Constant
hi! link cssUIAttr Constant
hi! link cssCommonAttr Constant
hi! link Debug		PreProc	
hi! link Define		PreProc
hi! link Delimiter	Identifier
hi! link Directory	Statement
hi! link ErrorMsg	Error
hi! link Exception	Statement
hi! link Float		Constant
hi! link FoldColumn	Folded
hi! link Function	Identifier
hi! link Function	Identifier
hi! link Ignore		Comment
hi! link Include		PreProc
hi! link cIncluded		Comment
hi! link IncSearch	Search
hi! link Keyword		Identifier
hi! link Label		Statement
hi! link MoreMsg		Identifier
hi! link NonText		Comment
hi! link Number		Constant
hi! link Operator	Identifier
hi! link Question	MoreMsg
hi! link phpStructure Conditional
hi! link PreCondit	PreProc
hi! link Repeat		Statement
hi! link SignColumn	Identifier
hi! link SpecialChar	Special
hi! link SpecialComment	Special
hi! link SpecialKey	Special
hi! link SpellBad	Error
hi! link SpellCap	Error
hi! link SpellLocal	Error
hi! link SpellRare	Error
hi! link StorageClass	Type
hi! link StorageClass	Normal
hi! link String		Constant
hi! link Structure  PreProc
hi! link Title		Structure
hi! link Todo		Error
hi! link Typedef		PreProc
hi! link WarningMsg	Error
