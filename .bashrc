#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# function to print out git branch when in git dir
function parse_git_branch {

  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/[\1]/'

}

# function to print out mercurial branch when in hg dir
__hg_ps1 ()
{
    if [ "$(hg root 2> /dev/null)" ]; then
        printf "$1" "$(hg branch)"
    fi
}

PS1='\n\[\e[1;34m\]\t\[\e[0m\e[1;30m\]:\[\e[0m \e[1;31m\]\u\[\e[0m \e[1;30m\]on\[\e[0m \e[1;36m\]\h\[\e[0m \e[1;30m\]at\[\e[0m \e[1;33m\]\w\[\e[0m \e[1;32m\]$(parse_git_branch)$(__hg_ps1 "[%s]")\[\e[0m\]\n\[\e[1;30m\]>\[\e[0m\]'

alias ls='ls --color=auto'

export TERM=xterm-256color
