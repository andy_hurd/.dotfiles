set nocompatible                  " full vim
syntax enable
set encoding=utf8

call pathogen#infect()            " load pathogen
filetype plugin indent on

let mapleader=","

" autocmd VimEnter * NERDTree     " automatically open NERDTree on vim open
" autocmd VimEnter * wincmd p     " after NERDTree open, jump to opened file

map <leader>n :NERDTreeToggle<CR>
let NERDTreeQuitOnOpen=1          " close NERDTree when open file
let NERDTreeShowHidden=1          " show hidden files in NERDTREE

" fast window switching
map <leader>, <C-w><C-w>

set scrolloff=3                   " show 3 lines of context around cursor
set autoread                      " auto read when a file modified externally
set autowrite

set ruler                         " always show current position

set hlsearch                      " highlight search things
set incsearch                     " go to search results as typing
set smartcase                     " case-sensative if search has capital letter
set ignorecase                    " ignore case when searching
set gdefault                      " assume global when searching
set showmatch                     " show matching brackerts text hover

set nobackup                      " prevent backups
set nowritebackup
set noswapfile

set shiftwidth=2
set softtabstop=2
set tabstop=2

set smarttab
set expandtab                     " use spaces, not TAB
set autoindent                    " set automatic code indentation

set nowrap                        " don't wrap lines

set noeb vb t_vb=                 " disable audio and visual bells

set t_Co=256                      " use 256 colors
colorscheme dapper

if exists('+colorcolumn')
  set colorcolumn=80              " show a right margin column
endif

set number                        " add line numbers

" FOLDING
set foldenable                    " enable folding
set foldmethod=indent             " most files evenly indented
set foldlevel=99

" ADDITIONAL KEY MAPPINGS

" fast escaping
imap jk <Esc>

" auto complete {} indent and position the cursor in the middle line
inoremap {<CR> {<CR>}<Esc>O

" copy/paste to/from x clipboard
vmap <leader>y :!xclip -f -sel clip<CR>
map <leader>p :r!xclip -o<CR>

set rtp+=$HOME/.local/lib/python2.7/site-packages/powerline/bindings/vim/

" Always show statusline
set laststatus=2

" map <leader>m :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
"      \ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
"      \ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>
